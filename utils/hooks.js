import {useState} from "react";

export function useToggle(initialState = false) {
    const [toggle, setToggle] = useState(initialState)

    const handleReset = () => setToggle(initialState)

    const handleToggle = () => setToggle(prevState => !prevState)

    return {
        on: toggle,
        set: setToggle,
        reset: handleReset,
        toggle: handleToggle
    }
}

export function useLocalStorage(keyName, initialValue) {
    const [storedValue, setStoredValue] = useState(() => {
        try {
            if (typeof window === 'undefined') {
                return initialValue
            }

            const item = window.localStorage.getItem(keyName)

            return item !== null ? JSON.parse(item) : initialValue;
        } catch (error) {
            console.log(error)

            return initialValue
        }
    })

    const setValue = (value) => {
        try {
            setStoredValue(value)

            window.localStorage.setItem(keyName, JSON.stringify(value))
        } catch (error) {
            console.log(error)
        }
    }

    return [storedValue, setValue]
}

