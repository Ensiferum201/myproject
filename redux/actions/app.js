import * as t from "../types"

export const addCount = count => ({
    type: t.SET_PAGE_LOADING,
    count
})
