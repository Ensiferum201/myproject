import { DEBUG } from '../config'
import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { composeWithDevTools } from 'redux-devtools-extension'
import { createWrapper } from "next-redux-wrapper"
import rootReducer from './reducers/rootReducer'

const middleware = [thunk]

const makeStore = () => createStore(
    rootReducer, 
    DEBUG 
        ? composeWithDevTools(applyMiddleware(...middleware)) 
        : applyMiddleware(...middleware)
)

const store = makeStore()

const storeFunc = () => store

export const { dispatch } = store
export const wrapper = createWrapper(storeFunc)