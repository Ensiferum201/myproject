import { storeInitial } from '../storeInitial'
import * as t from "../types"

const app = (state = storeInitial.app, action) => {
    switch (action.type) {
        case t.SET_PAGE_LOADING:
            return {
                ...state,
                setCount: state.setCount + action.count
            }

        default:
            return { ...state }
    }
}

export default app
