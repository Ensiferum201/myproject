import {useLocalStorage, useToggle} from "../utils/hooks";

export default function PageInner() {
    const { on, set, reset, toggle } = useToggle()
    const [value, setValue] = useLocalStorage('name', 'Joe')

    const style = {
        display: 'flex',
        flexDirection: 'column',
        marginTop: '60px'
    }

    return (
        <div style={style}>
            <div>
                <p>On: {on ? 'true' : 'false'}</p>

                <button onClick={() => set(true)}>Set to on</button>
                <button onClick={reset}>Reset</button>
                <button onClick={toggle}>Toggle</button>
            </div>

            <div>
                <p>{value}</p>

                <input type="text" onChange={(e) => setValue(e.currentTarget.value)} />
            </div>
        </div>
    )
}
